package com.simplon.p16.poo;

public class Validator {

    private int minChar = 2;
    private int maxChar = 32;
    public Validator(){
        this.minChar = 2;
        this.maxChar = 32;
    }
    public Validator(int minChar , int maxChar) {
        this.minChar = minChar;
        this.maxChar = maxChar;
    }

    public boolean isCorrectLenght(String inputString){
        int stringLength = inputString.length();
        boolean minMaxChar = stringLength > minChar && stringLength < maxChar;
        return minMaxChar;
    }

    public boolean haveTrailingSpace(String inputString){
        return (inputString.endsWith(" ") || inputString.startsWith(" "));
    }
    public boolean haveSpecialChars(String inputString){
        return (inputString.contains("_") || inputString.contains("!") || inputString.contains("-"));
    }
    public boolean validate (String inputString){
        
        return (this.isCorrectLenght(inputString) && !this.haveSpecialChars(inputString) && !this.haveTrailingSpace(inputString));
    }
    public boolean isCorrectPassword(String inputString){

        boolean inputLenght = inputString.length()>4;
        boolean isOneUppercaseLettre = false;
        boolean isOneNumber = false;

        for (int i=0; i< inputString.length(); i++) {
            if (Character.isUpperCase(inputString.charAt(i))){
                isOneUppercaseLettre = true;
            }
        }

        for (int i=0; i< inputString.length(); i++) {
            if (Character.isDigit(inputString.charAt(i))){
                isOneNumber = true;
            }
        }
        return (inputLenght && isOneNumber && isOneUppercaseLettre);

    }
    public boolean isCorrectEmail(String inputString){
        String pattern = "^[a-zA-Z0-9-_.]+@[a-zA-Z0-9]+\\.[a-z]{2,6}$";
        return (inputString.matches(pattern));
    }

    public boolean isCorrectPasswordRegex(String inputString){
        String Pattern = "(.{4,})([A-Z]";


    }
}
