package com.simplon.p16.poo;

public class Calculator {

    public int value1 = 2;
    public int value2 = 2;

    public void add(int a, int b) {
        System.out.println(a + b);
    }

    public void substratc(int a, int b) {
        System.out.println(a - b);
    }

    public void multiply(int a, int b) {
        System.out.println(a * b);
    }

    public void divise(int a, int b) {
        try {
            int result = a / b;
            System.out.println(result);
        } catch (ArithmeticException e) {
            System.out.println("division par 0 impossible!");
        }

    }

    public void addAttribut() {
        System.out.println(value1 + value2);
    }

    public void subtractAttribut() {
        System.out.println(value1 - value2);
    }

    public void multiplyAttribut() {
        System.out.println(value1 * value2);
    }

    public void diviseAttribut() {
        try {
            int result = value1 / value2;
            System.out.println(value1 / value2);
        } catch (ArithmeticException e) {
            System.out.println("division par 0 impossible");
        }

    }

    public void name() {

    }

    public Integer calculateWithInputString(int a, int b, String operator) {

        switch (operator) {
            case "+":
                return a + b;
            case "-":
                return a - b;

            case "*":
                return a * b;

            case "/":
                try {
                    return a / b;
                } catch (ArithmeticException e) {
                    System.out.println("division par 0 impossible!");
                    break;
                }
            default:
                System.out.println("opérateur inconnu!");
                return null;
        }
        return null;

    }

    public int calculatorWithInputStringIfElseMode(int a, int b, String operator) {
        int result = 0;
        if (operator == "+") {
            result = a + b;
        } else if (operator == "-") {
            result = a - b;
        } else if (operator == "*") {
            result = a * b;
        } else if (operator == "/") {
            result = a / b;
        } else {
            System.out.println("invalid enter");
        }
        System.out.println(result);
        return result;
    }

    public boolean isEven(int a){
        return a%2 == 0;
        }

}
